The install got to 46%, waited a while and then froze the computer.
* Partway down this [Nixos discourse article](https://discourse.nixos.org/t/install-freezes-at-46/22892) a contributor voices the suspicion that this may be a RAM issue (the optiplex in question has 1.9GB RAM).
* A [reddit post](https://www.reddit.com/r/NixOS/comments/v72u8w/correctly_trying_to_install_on_wiped_ssd_install/) linked me to [the NixOS install guide](https://nixos.org/manual/nixos/stable/index.html#sec-installation-installing) which indicates that the solution to this problem may be to turn on swap on the ssd drive before installing.
The swap has to be created and activated in the live boot session so that the installer can access it. However, it needs to be added on the drive of the target machine so it is available to the installation after the install.
### Steps
I used GParted to create a swap partition (with the swap flag turned on), but creating a swapfile on the root would have also worked.
`sudo mkswap /partitionname` (I got the partition name from gparted. it was /dev/_something_.)
`sudo swapon /partitionname`
The NixOS installer adds preexisting swap to the hardware.nix file, so it'll remain available after reboot and through subsequent rebuilds.
### Resources
[How Much Swap Should You Use in Linux? (itsfoss.com)](https://itsfoss.com/swap-size/) explains why 2GB is a reasonable swap size for most linux installations
[How to Increase Swap Space in Linux (www.baeldung.com)](https://www.baeldung.com/linux/increase-swap-space) details the steps for adding swap

