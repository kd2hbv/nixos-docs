## Update the targeted version
Tips from a  [superuser.com](https://superuser.com/questions/1604694/how-to-update-every-package-on-nixos) article:

Two links that reference the latest version of nixos:
1. https://nixos.org/manual/nixos/stable/#sec-upgrading
2. https://status.nixos.org/

To show what channel you are currently on: `sudo nix-channel --list`.

To remove the current channel: `sudo nix-channel --remove nixos`.

To add the new channel: `sudo nix-channel --add https://nixos.org/channels/nixos-21.11 nixos` (with the correct version, of course).

To complete: `nixos-rebuild switch --upgrade`.
## Fixing broken tool links
In xfce the webbrowser shortcut will need updating to point to the new nixstore path of your webbrowser.  

To find that path (assuming the brave browser), `cd /nix/store` and `ls * | grep "brave"`.  Look in the results for the actual app (as opposed to `.drv` files, etc.),  copy the store address and paste it into the webbrowser definition in settings - default applications.
