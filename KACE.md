https://support.quest.com/kb/4272341/how-to-find-and-install-the-generic-linux-agent-for-sma (requires an [id.quest.com](https://id.quest.com/auth/realms/quest/login-actions/registration?client_id=support-quest-com&tab_id=4wLuiLRPV6I) login to access) says:

>The generic Linux SMA agent packages are only available to verified customers in the relevant [ITNinja Community](https://www.itninja.com/community/kace-systems-management-appliance) (under the **Customer Files** tab in the community).

To download the .tar.gz, you have to
* Create an [ITNinja login](https://www.itninja.com/login), and
* Link it to a valid "customer support account email address" [here](https://www.itninja.com/profile/settings).

The "How To" article continues with steps about setting up and configuring the service.