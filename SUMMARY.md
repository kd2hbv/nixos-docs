# Table of contents

* [Home](README.md)
* [HamTop setup](HamTop.md)
* [Updating Nix Version](UpdatingNixVersion.md)
* [Citrix Workspace App](CWA.md)
* [BitDefender](BitDefender.md)
* [KACE Agent](KACE.md)
* [Repurpose Optiplex 780](RepurposeRetiredDellOptiplex780.md)
