# Background
The "HamTop" is a Toshiba "Swanky" Chromebook.
## Prepare the installation media
1. Set up a [USB ventoy stick](https://www.ventoy.net/en/index.html).
2. Download the current [NixOs installer](https://nixos.org/download.html) onto the ventoy stick.
## Boot off the installation media
1. Boot off the ventoy stick and select Nixos.
## Pre-installation preparation
1. If installation wizard auto-runs, cancel it.
2. Connect the wifi
## Prepare the media partitions
Run gparted.
1. If initial install:
	1. The onboard storage should have a gpt partition type table with two partitions:
		1. 512MB FAT32 partition for EFI boot
			1. Should have the EFS and boot options ticked
			2. label it EFI
		2. Remainder BTRFS for the home partition
			1. label it HamTop
	2. The additional SSD should be a single ext4 partition, labeled HamCard
2. If a reinstall:
	1. Reformat the 512MB FAT32 EFI partition on the on-board storage.
	2. Reformat the ext4 partition on the additional SSD
## Get internet access
Open a browser and authenticate to [https://cache.nixos.org/](https://cache.nixos.org/). 
Keep this page open during the installation and refresh it every couple of minutes so that the network connection stays alive. 
## Run the installer
Open the installer. Points of note:
1. Location: US/NY
2. Keyboard: English Dvorak.
3. User:
	- Full name
	- Ham license as login name
	- tick *use the same password for the administrator account*
4. Desktop: xfce
5. Unfree software: Allow (this will be necessary for installing citrix workspace app)
6. Select the manual partition option:
	- Map the FAT32 EFI bootloader partition to mount at `/boot/efi` and make sure that boot option is ticked.
			- The version 23.05 installer only listed `/boot` in the mountpoint list.
			- If this happens again, type `/boot/efi` manually. 
	- Map the ext4 OS partition (on the HamCard) to mount at `/`
	- Map the home partition (btrfs partition on HamTop) to mount at `/home`
7. Install
## Post-reinstall cleanup
1. Run `sudo nix-store --optimise` This can take a *very* long time..... Be patient!
3. See files that are preventing garbage collection:
	1. Run `nix-store --gc --print-roots`
	2. `rm` any files that are in `\results` folders
4. Garbage collection sequence:
	1. Run `sudo nix-env --delete-generations old`
	2. Run `sudo nix-collect-garbage -d`
	4. Run `sudo reboot now`
## Rebuild
Open a browser and authenticate to [https://cache.nixos.org/](https://cache.nixos.org/). 
Keep this page open during the rebuild and refresh it every couple of minutes so that the network connection stays alive. 
1. The `nixos-config` git repo should be in the user's home folder.
	1. If not, clone it from GitLab.
2. There should be a simlink to the `nixos-config` git repo under `/etc/nixos/`.
	1. If there isn't add it.  The simplest way is to create a shortcut to the repo on the desktop, then move that shortcut to `/etc/nixos`.
3. Replace the nix configuration file created at installation with the one from the repo:
	1. Run `sudo mv /etc/nixos/configuration.nix /etc/nixos/configuration.nix.bak`
	2. Run `sudo cp /etc/nixos/nixos-config/configuration.nix /etc/nixos/configuration.nix`
4. Run the `/etc/nixos/nixos-config/scripts/citrix-prefetch.sh` script.
5. Then rebuild the system:
	- Run `sudo nixos-rebuild switch`
	- Run `sudo reboot now`
6. Make changes to `configuration.nix` as needed.
	1. If changed, copy it back down to `/etc/nixos/nixos-config` after successful build.

If the build fails with a message similar to the following, see [this page](https://discourse.nixos.org/t/solved-23-05-upgrade-nixos-rebuild-switch-fails/28625):

>`sudo nixos-rebuild switch`
> `building Nix...`
> `building the system configuration...`
> `Traceback (most recent call last): `
> `  File "/nix/store/y8pzl5487as5vi1594r39fi9wpjby2pc-systemd-boot", line 341, in <module> main() File "/nix/store/y8pzl5487as5vi1594r39fi9wpjby2pc-systemd-boot", line 262, in main installed_out = subprocess.check_output(["/nix/store/75wxj2a3c0pdbf46bzmff8qr9vbjm5y1-systemd-253.3/bin/bootctl", "--esp-path=/boot/efi", "status"], universal_newlines=True)`
> `  File "/nix/store/95cxzy2hpizr23343b8bskl4yacf4b3l-python3-3.10.11/lib/python3.10/subprocess.py", line 421, in check_output return run(*popenargs, stdout=PIPE, timeout=timeout, check=True, `
> `  File "/nix/store/95cxzy2hpizr23343b8bskl4yacf4b3l-python3-3.10.11/lib/python3.10/subprocess.py", line 526, in run raise CalledProcessError(retcode, process.args, `
> `subprocess.CalledProcessError: Command '['/nix/store/75wxj2a3c0pdbf46bzmff8qr9vbjm5y1-systemd-253.3/bin/bootctl', '--esp-path=/boot/efi', 'status']' returned non-zero exit status 1.`
> `warning: error(s) occurred while switching to the new configuration`

Otherwise, if it fails with other messages about about missing `boot/efi`, see [this nix page](https://nixos.wiki/wiki/Bootloader).

On successful build
	1. Run `sudo reboot now` to properly restart the machine.
	2. Run the garbage collection
		1. Run `cd /etc/nixos/nixos-config/scripts`
		2. Run `./remove-garbage.sh`
	3. Run `sudo cp /etc/nixos/configuration.nix /etc/nixos/nixos-config/configuration.nix` to backup the current config
	4. Run `sudo rm /etc/nixos/configuration.nix.bak` to remove the no longer needed backup of the installation created configuration.

If all is good, the toolbar should show the right apps afterwards.